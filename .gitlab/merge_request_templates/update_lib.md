# Update library

To update the version follow these steps:

- [ ] Update the `VERSION` file with the latest available version from
[https://github.com/jshttp/mime-db/releases](https://github.com/jshttp/mime-db/releases).
- [ ] Run the following command to generate the latest types

  ```sh
  go run ./cmd/generate

  # outputs generated_mime_types.go
  ```

- [ ] Commit the changes and push
- [ ] Assign to reviewer
