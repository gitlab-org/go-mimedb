# Release a new version

- [ ] Set the milestone on this issue
- [ ] Optional. If you want to update this library with the latest
available version from [https://github.com/jshttp/mime-db/releases](https://github.com/jshttp/mime-db/releases)
open a merge request and follow [this template](../merge_request_templates/update_lib.md)
- [ ] Once your MR has been merged, [create a new pipeline](https://gitlab.com/gitlab-org/go-mimedb/-/pipelines/new)
for the default branch
- [ ] Expect the [`release`](../ci/release.yml) stage to complete successfully
  - If the `release` job fails due to `fatal: tag 'vX.Y.Z' already exists`,
  you will need to update the `VERSION` file first (see second step)
- [ ] Verify the release has been created under https://gitlab.com/gitlab-org/go-mimedb/-/releases


/label ~backend ~"devops::release" ~"group::release management"
