package main

import (
	"fmt"
	"mime"

	"gitlab.com/gitlab-org/go-mimedb"
)

func main() {
	for t, extensions := range mimedb.MimeTypeToExts {
		registredExtensions, err := mime.ExtensionsByType(t)
		if err != nil {
			fmt.Printf("type: %s not found!\n", t)
			continue
		}

		diff := diffExtensions(registredExtensions, extensions)
		if len(diff) > 0 {
			fmt.Printf(`%s
	OS extensions: %v
	mimedb extensions: %v
	difference: %v
`,
				t,
				registredExtensions,
				extensions,
				diffExtensions(registredExtensions, extensions))
		}
	}
}

func diffExtensions(registredExtensions, extensions []string) []string {
	result := []string{}

	for _, ext := range extensions {
		if !contains(registredExtensions, "."+ext) {
			result = append(result, ext)
		}
	}

	return result
}

func contains(list []string, value string) bool {
	for _, v := range list {
		if v == value {
			return true
		}
	}
	return false
}
