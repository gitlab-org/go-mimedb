# MIME DB

This Go package uses generators to convert [this database](https://github.com/jshttp/mime-db)
into additions to the stdlib `mime` package.

Since all the work is done at compile time, the MIME types end up embedded in
the binary, loading them on startup is fast, and you still get sensible results
when `/etc/mime.types` is unavailable on your platform!

This work is somewhat inspired by [mime-ext-go](https://github.com/mytrile/mime-ext-go),
which lacks the automatic generation (and so easy update) to be found in this
package.

The version of the mime-db package used is tracked in the VERSION file, and
updates will be given a corresponding tag.

## Contributing

See the [contributing guide](CONTRIBUTING.md)

## How to update

Update the version number in [VERSION](./VERSION) to the latest version available in
[jshttp/mimedb](https://github.com/jshttp/mime-db). Then run:

```sh
go run ./cmd/generate

# outputs generated_mime_types.go
```

### Failing tests

If the tests are failing with a message like:

```console
Mime Type 'application/toml' already registred for extension 'toml' without loading the database!
```

The latest version of the OS might have caught up with the MIME database which makes the test case
outdated. To find a MIME type that exist in `mimedb` but doesn't exist in OS, you can run
`go run ./cmd/list/main.go`, which lists the `mimedb` types and extensions.

## How to update go

1. Update the go version in `go.mod`
1. Update the `GO_VERSION` matrix in `.gitlab/ci/test.yml`
