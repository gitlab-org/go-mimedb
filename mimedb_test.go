package mimedb

import (
	"mime"
	"testing"
)

const (
	extension    = ".toml"
	expectedType = "application/toml"
)

func TestLoadTypes(t *testing.T) {
	foundType := mime.TypeByExtension(extension)

	if foundType != "" {
		t.Logf("Mime Type '%s' already registred for extension '%s' without loading the database!", foundType, extension)
		t.Fail()
	}

	if err := LoadTypes(); err != nil {
		t.Logf("Unexpected error loading types: %s", err)
		t.Fail()
	}

	foundType = mime.TypeByExtension(extension)
	if foundType != expectedType {
		t.Logf("Different MIME type found for '%s' extension. '%s' was found, '%s' was expected", extension, foundType, expectedType)
		t.Fail()
	}
}
